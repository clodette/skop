<?php
require_once('clases/model.php');


$fichero_name = readline("Ingrese el nombre del archivo: ");

if (file_exists(getcwd().'/'.$fichero_name))
{
  $fichero_path = getcwd().'/'.$fichero_name;
  $users = json_decode(file_get_contents($fichero_path), true);

  foreach ($users['users'] as $key => $value) {
    $user_fields= [];
    $user_fields['avatar_path'] = $value['avatar'];
    $user_fields['name'] = $value['name'];
    $user_fields['lastname'] = $value['lastname'];
    $user_fields['gender'] = $value['gender'];
    $user_fields['password'] = $value['password'];
    $user_fields['birthday'] = $value['date'];
    $user_fields['hour'] = $value['hour'];
    $user = new User($user_fields);
    $user->save();
  }
  echo "Exito!!!";
}
else {
  echo "El archivo no existe ";
  exit;
}
?>
