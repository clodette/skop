<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/theme.css">

    <title>Skop.com</title>
  </head>


  <body class="<?=CURRENT_SECTION?>">

    <header>

        <nav class="navbar navbar-expand-md fixed-top violet_bg transparent">
            <a class="navbar-brand white" href="index.php">SKOP.COM</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
              <span class="fas fa-bars white"></span>
            </button>
            <div class="collapse navbar-collapse mr-auto" id="navbarCollapse">

              <form class="form-inline mt-2 mt-md-0 white_bg buscador">
                <input class="form-control mr-sm-2" type="text" placeholder="Buscar" aria-label="Search">
                <button class="btn btn-link violet my-2 my-sm-0" type="submit"><i class="fas fa-search violet"></i></button>
              </form>
              <ul class="navbar-nav ml-4">
                <!--<li class="nav-item active ml-2">
                  <a class="nav-link violet white_bg circle" href="#"><i class="fas fa-arrow-up violet"></i></a>
                </li>
                <li class="nav-item ml-2">
                  <a class="nav-link violet white_bg circle" href="#"><i class="fas fa-bell violet"></i></a>
                </li>-->
                <li class="av-item ml-2 d-flex">
                  
                  <?php if(isset($_SESSION['user'])):?>
                    <?php $file_dir = $_SESSION['user']['avatar']; ?>
                    <figure class="avatar circle mr-2 mb-0">
                      <?php echo "<img class='img-fluid' src='$file_dir'/>";?>
                    </figure>
                    <p class="white m-0 text-uppercase">
                      <strong>Hola <?php echo "{$_SESSION['user']['name']}";?>!</strong> 
                      <a class="white" href="logout.php"><i class="fas fa-sign-out-alt white"></i> </a></p>
                      <?php elseif(!isset($_SESSION['user'])) :?>
                      <p class="white m-0 text-uppercase go_login">
                        <strong>Hola! debes <a class="white" href="login.php">loguearte para participar <i class="fas fa-sign-in-alt white"></i> </a> </strong></p>
                  <?php endif; ?>
                </li>
              </ul>
            </div>
        </nav>
    </header>
