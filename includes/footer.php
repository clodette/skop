<!-- FOOTER -->
<footer class="container-fluid white_bg">
    <p class="deepBlue text-center m-0 pb-2">¿Querés saber más sobre <em class="normal bold text-uppercase">skop.com</em>? Cliqueá <a href="faqs.php" class="violet" title="Preguntas frecuentes">aquí</a></p>
</footer>
<div class="chatMobile d-block d-lg-none">
    <button type="button" class="btn btn-light circle"><i class="fas fa-comments violet fa-lg"></i></button>
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>