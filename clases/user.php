<?php
require_once('db.php');
require_once('model.php');

class User extends Model
{
  public $name;
  public $lastname;
  public $email;
  public $gender;
  public $password;
  public $birthday;
  public $hour;
  public $avatar_path;

  public $fillable = ['name', 'lastname', 'email', 'gender', 'password', 'birthday', 'hour', 'avatar_path'];
  public $table = 'users';

}

?>
