<?php
require_once('db.php');
require_once('user.php');
require_once('model.php');

class Image
{


public function insertImage($photo, $user, $source)
  {
    if (array_key_exists($photo, $source))
    {
      $file = $source[$photo];
      $name = $file['name'];
      $tmp = $file['tmp_name'];
      $ext = pathinfo($name, PATHINFO_EXTENSION);

      $directory = "../fotos/";

      if(!file_exists($directory))
      {
        $old = umask(0);
        mkdir($directory, 0777);
        umask($old);
      }

      $date = new DateTime();

      $finalPhoto = $directory .$user. "_imagen_".$date->getTimestamp()."." . $ext;

      move_uploaded_file($tmp, $finalPhoto);

      return $finalPhoto;
    }

  }
}

?>
