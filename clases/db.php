<?php


class DB
{
  private static $connection;

  public static function getConnection()
  {
    if(!self::$connection){
      $db = new PDO('mysql:host=localhost;port=3306;dbname=skop', 'root', 'root');
      $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      self::$connection = $db;
    }

    return self::$connection;
  }
}







?>
