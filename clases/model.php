<?php
require_once('db.php');
require_once('user.php');
//require_once('image.php');

class Model
{

  public function __construct($data)
  {
    foreach ($data as $key => $value)
    {
      if (in_array($key, $this->fillable))
      {
        $this->$key = $value;
      }
    }
  }

  public function find($field, $value)
  {
    $sql = 'SELECT * FROM users WHERE '.$field.' = :value';
    $stmt = DB::getConnection()->prepare($sql);
    $stmt->bindValue(':value', $value, PDO::PARAM_STR);
    $stmt->execute();
//
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    return $result;

  }



  public function save(){
    $sql = $this->insert();
    $stmt = DB::getConnection()->prepare($sql);
    foreach ($this->fillable as $column)
    {
      $stmt->bindValue(':$column', $this->$column);
    }
    $stmt->execute();
  }


  public function setPassword($pass){
    return password_hash($pass, PASSWORD_DEFAULT);
  }


  private function insert()
  {
    $columns = implode(', ', $this->fillable);
    $values = '';
    foreach ($this->fillable as $key)
    {
      if ($key == 'password') {
        $values = $values."'".$this->setPassword($this->$key)."',";
      } else {
        $values = $values."'".$this->$key."',";
      }
    }
    $values = trim($values, ',');
    return "INSERT INTO ".$this->table." ($columns) VALUES($values)";
  }
}


?>
