<?php
define( 'CURRENT_SECTION', 'registro' );
include('globales.php');
require_once('clases/user.php');
require_once('clases/image.php');
require_once('clases/model.php');

//INICIO DE SESION
session_start();


if (parameterExist('user', $_SESSION)){
  header("Location: index.php");
  exit;
}

$initialUser = [
  'avatar_path' =>  NULL,
  'name' => returnInfo('name', $_POST),
  'lastname' => returnInfo('lastname', $_POST),
  'email' => returnInfo('email', $_POST),
  'gender' => returnInfo('gender', $_POST),
  'password' => returnInfo('password', $_POST),
  'confirm_password' => returnInfo('confirm_password', $_POST),
  'birthday' => returnInfo('birthday', $_POST),
  'hour' => returnInfo('hour', $_POST),
];

$image = new Image();

if(isImageOk('photo')) {
  $initialUser['avatar_path'] = $image->insertImage('photo', $initialUser['name'], $_FILES);
  //$initialUser['avatar_path'] = saveFile('photo', $initialUser['name']);
}

$error = false;
$user = NULL;

if (parameterExist('submit', $_POST)){
  if ($initialUser['name'] != null && $initialUser['lastname'] != null && $initialUser['email']!= null && $initialUser['gender']!= null && $initialUser['password'] != null && $initialUser['confirm_password']!= null && $initialUser['birthday'] != null && $initialUser['hour'] != null  && $initialUser['confirm_password'] == $initialUser['password']){
    $user = new User($initialUser);
    //var_dump($user->find($user->email));

    }else{
        $error = true;
    }
};

$error_exist = false;

 if($user != NULL) {
   if (!$user->find('email', $user->email))
   {
     $user->save();
     //$_SESSION['user'] = $initialUser;
     header("Location: login.php");
     exit;
   }
   else
   {
     $error_exist = true;
   }
 }

//FORMULARIO HTML

include("includes/header.php");
?>
    <main role="main">

        <div class="container mainContent d-flex flex-column justify-content-center align-items-center">
            <section class="registerForm col-lg-10 col-sm-12">
                <header>
                    <h1 class="gray-dark text-center">Registro</h1>
                </header>
                <div class="card p-4">
                    <p class="text-center ifgp lead m-0  deepBlue"><em>Completa el siguiente formulario y crea una cuenta en la web.</em></p>
                    <p class="text-center ifgp lead  deepBlue"><em>Skop te permite conocer gente por afinidad astral.</em></p>

                    <?php if($error_exist): ?>

                      <div class="col-md-12 alerta">
                        El usuario ya existe. <a href="login.php"><strong class="violet">Inicia sesión aquí</strong></a>
                      </div>

                    <?php endif; ?>

                    <form class="mt-2" method="post" enctype="multipart/form-data">
                        <div class="row">
                          <div class="col-md-12 form-group">

                              <label for="avatar_path" class="text-uppercase violet">Subir Avatar</label>
                              <?php
                                $error_class = '';
                                if($error && !$initialUser['avatar_path']) {$error_class = 'error_class';};
                              ?>

                              <input type="file" class="form-control <?=$error_class?>" id="name" name="photo">
                              <?php if($error && !$initialUser['avatar_path']):?>
                                <span class="error_message">Sube una imagen</span>
                        			<?php endif; ?>
                          </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label for="user" class="text-uppercase violet">Nombre *</label>
                                <?php
                                  $error_class = '';
                                  if($error && !$initialUser['name']) {$error_class = 'error_class';};
                                ?>
                                <input type="text" class="form-control <?=$error_class?>" id="name" name="name" value="<?=$initialUser['name']?>">
                                <?php if($error && $initialUser['name'] == null):?>
                          				<span class="error_message">Ingrese su nombre</span>
                          			<?php endif; ?>
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="lastname" class="text-uppercase violet">Apellido *</label>
                                <?php
                                  $error_class = '';
                                  if($error && !$initialUser['lastname']) {$error_class = 'error_class';};
                                ?>
                                <input type="text" class="form-control <?=$error_class?>" id="lastName" name="lastname" value="<?= $initialUser['lastname']?>">
                                <?php if($error && $initialUser['lastname'] == null):?>
                                  <span class="error_message">Ingrese su apellido</span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label for="email" class="text-uppercase violet">Email *</label>
                                <?php
                                  $error_class = '';
                                  if($error && !$initialUser['email']) {$error_class = 'error_class';};
                                ?>
                                <input type="email" class="form-control <?=$error_class?>" id="email" name="email" value="<?= $initialUser['email']?>">
                                <?php if($error && $initialUser['email'] == null):?>
                          				<span class="error_message">Ingrese su email</span>
                          			<?php endif; ?>
                            </div>
                            <div class="col-md-6 form-group">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="gender" value="Mujer" id="defaultCheck1">
                                    <label class="form-check-label text-uppercase violet" for="defaultCheck1">
                                        Mujer
                                    </label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="gender" value="Hombre" id="defaultCheck2">
                                    <label class="form-check-label text-uppercase violet" for="defaultCheck2">
                                       Hombre
                                    </label>
                                </div>
                                <?php if($error && $initialUser['gender'] == null):?>
                                  <span class="error_message">Ingrese su genero</span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label for="password" class="text-uppercase violet">Contraseña *</label>
                                <?php
                                  $error_class = '';
                                  if($error && !$initialUser['password']) {$error_class = 'error_class';};
                                ?>
                                <input type="password" class="form-control <?=$error_class?>" id="password" name="password" value="">
                                <?php if($error && $initialUser['password'] == null):?>
                                  <span class="error_message">Ingrese su password</span>
                                <?php endif; ?>
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="password" class="text-uppercase violet">Confirmar contraseña *</label>
                                <?php
                                  $error_class = '';
                                  if($error && !$initialUser['confirm_password']) {$error_class = 'error_class';};
                                ?>
                                <input type="password" class="form-control <?=$error_class?>" id="repassword" name="confirm_password" value="">
                                <?php if($error && $initialUser['confirm_password'] == null):?>
                                  <span class="error_message">Ingrese su confirmar password</span>
                                <?php elseif($initialUser['confirm_password'] != $initialUser['password']):?>
                                  <span>La contraseña no es igual</span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 form-group">
                                <label for="birthday" class="text-uppercase violet">Fecha de nacimiento *</label>
                                <?php
                                  $error_class = '';
                                  if($error && !$initialUser['birthday']) {$error_class = 'error_class';};
                                ?>
                                <input type="date" class="form-control <?=$error_class?>" id="birthday" name="birthday" value="<?= $initialUser['birthday']?>">
                                <?php if($error && $initialUser['birthday'] == null):?>
                                  <span class="error_message">Ingrese su fecha de nacimiento</span>
                                <?php endif; ?>
                            </div>
                            <div class="col-md-6 form-group">
                                <label for="birthHour" class="text-uppercase violet">Hora de nacimiento *</label>
                                <?php
                                  $error_class = '';
                                  if($error && !$initialUser['hour']) {$error_class = 'error_class';};
                                ?>
                                <input type="time" class="form-control <?=$error_class?>" id="birthHour" name="hour" value="<?=$initialUser['hour']?>">
                                <?php if($error && $initialUser['hour'] == null):?>
                                  <span class="error_message">Ingrese su hora de nacimiento</span>
                                <?php endif; ?>
                            </div>
                        </div>
                        <div class="row text-center d-flex justify-content-center align-items-center">
                            <button type="submit" class="btn btn-primary gold_bg gold_border" name="submit">Registrarse</button>
                        </div>
                    </form>
                </div>
                <hr class="separator" style= "margin-top: 3rem"/>
                <p class="purple text-center">¿Ya tenés cuenta?  <a href="login.php" class="violet">Inicia sesión aquí</a></p>
            </section>
        </div>

</main>
        <?php
    include("includes/footer.php");
?>
