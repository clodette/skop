<?php
   define( 'CURRENT_SECTION', 'home' );
   include('globales.php');
   session_start();
   include("includes/header.php");

?>
    <main role="main">
        <section class="splash">
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <figure><img class="first-slide" src="img/galaxias/2.jpg" alt="First slide"></figure>
                        <div class="container">
                            <div class="carousel-caption text-left white">
                                <div class="interactions">
                                    <p class="white">16 <i class="fas fa-heart white ml-2 mr-4"></i> 24 <i class="fas fa-share-alt white  ml-2 mr-4"></i></p>
                                </div>
                                <h1 class="khand display-1">FORTUNA PARA HOY</h1>
                                <p class="ifgp lead"><em>Su trabajo fiel y diligente, Escorpio, además de muchos sacrificios por el bien de su carrera, pronto dará sus frutos.</em></p>
                                <p class="date"><small class="text-uppercase">15 de marzo</small></p>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <figure><img class="first-slide" src="img/galaxias/1.jpg" alt="First slide"></figure>
                        <div class="container">
                            <div class="carousel-caption text-left white">
                                <div class="interactions">
                                    <p class="white">16 <i class="fas fa-heart white ml-2 mr-4"></i> 24 <i class="fas fa-share-alt white  ml-2 mr-4"></i></p>
                                </div>
                                <h1 class="khand display-1">TIP SKOP</h1>
                                <p class="ifgp lead"><em>Asegurate de empezar tu dia con un rico y nutritivo desayuno! Hoy necesitaras energias adicionales, te espera un dia agotador!</em></p>
                                <p class="date"><small class="text-uppercase">15 de marzo</small></p>
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <figure><img class="first-slide" src="img/galaxias/3.jpg" alt="First slide"></figure>
                        <div class="container">
                            <div class="carousel-caption text-left white">
                                <div class="interactions">
                                    <p class="white">16 <i class="fas fa-heart white ml-2 mr-4"></i> 24 <i class="fas fa-share-alt white  ml-2 mr-4"></i></p>
                                </div>
                                <h1 class="khand display-1">HOY ES UN BUEN DIA PARA...</h1>
                                <p class="ifgp lead"><em>Su trabajo fiel y diligente, Escorpio, además de muchos sacrificios por el bien de su carrera, pronto dará sus frutos.</em></p>
                                <p class="date"><small class="text-uppercase">15 de marzo</small></p>
                            </div>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            <aside class="moreActions d-none d-sm-block">
                <ul class="list-unstyled">
                    <li class="white_border border_1 white pl-4 pr-4 mb-4 pt-1 pb-1 badge-pill">MAS TIPS PARA HOY <i class="fas fa-star"></i></li>
                    <li class="white_border border_1 white pl-4 pr-4 mb-4 pt-1 pb-1 badge-pill">PLANETAS <i class="fas fa-star"></i> </li>
                    <li class="white_border border_1 white pl-4 pr-4 mb-4 pt-1 pb-1 badge-pill">MATCHES PARA HOY <i class="fas fa-star"></i></li>
                    <li class="white_border border_1 white pl-4 pr-4 mb-4 pt-1 pb-1 badge-pill">RATING ESTRELLAS HOY <i class="fas fa-star"></i></li>
                </ul>
            </aside>
        </section>

        <div class="container mainContent">
            <section class="cta mb-4 row">
                <div class="col-12">
                    <div class="gold_bg p-1">
                        <ul class="list-unstyled d-flex flex-row m-0 justify-content-around">
                            <li class="white text-center">
                                <button type="button" class="btn btn-link white">
                                    <i class="fas fa-star d-block mb-2"></i>
                                    <span class="text-uppercase d-none d-sm-block">Horóscopo</span>
                                </button>
                            </li>
                            <li class="white text-center">
                                <button type="button" class="btn btn-link white">
                                    <i class="fas fa-magic d-block mb-2"></i>
                                    <span class="text-uppercase d-none d-sm-block">Tarot</span>
                                </button>
                            </li>
                            <li class="white text-center">
                                <button type="button" class="btn btn-link white">
                                    <i class="fas fa-question-circle d-block mb-2"></i>
                                    <span class="text-uppercase d-none d-sm-block">Quzzes</span>
                                </button>
                            </li>
                            <li class="white text-center">
                                <button type="button" class="btn btn-link white">
                                    <i class="fas fa-calculator d-block mb-2"></i>
                                    <span class="text-uppercase d-none d-sm-block">Calculadoras</span>
                                </button>
                            </li>
                            <li class="white text-center">
                                <button type="button" class="btn btn-link white">
                                    <i class="fas fa-heartbeat d-block mb-2"></i>
                                    <span class="text-uppercase d-none d-sm-block">Compatibilidad</span>
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="feedWrapper">
                <div class="row">
                    <aside class="col-4 d-none d-sm-block">
                        <article class="card p-4 gold_border text-center mb-4">
                            <h6 class="gold khand text-uppercase"><span class="gold_border border_1 d-inline-block pl-4 pr-4 pt-2 pb-2">Lectura recomendada</span></h6>
                            <h2 class="ifgp deepBlue">Numerología del 2018</h2>
                            <p class="deepBlue0">2018 - a 2 year - is an auspicious year in numerology, especially in relationships and alliances. What will it bring for you?</p>
                            <footer><a href="#" title="Leer mas" class="gold">Leer más <i class="fas fa-arrow-right"></i></a></footer>
                        </article>
                        <article class="card p-4 gold_border text-center mb-4">
                            <h6 class="gold khand text-uppercase"><span class="gold_border border_1 d-inline-block pl-4 pr-4 pt-2 pb-2">Lectura recomendada</span></h6>
                            <h2 class="ifgp deepBlue">Numerología del 2018</h2>
                            <p class="deepBlue0">2018 - a 2 year - is an auspicious year in numerology, especially in relationships and alliances. What will it bring for you?</p>
                            <footer><a href="#" title="Leer mas" class="gold">Leer más <i class="fas fa-arrow-right"></i></a></footer>
                        </article>
                        <article class="card p-4 gold_border text-center mb-4">
                            <h6 class="gold khand text-uppercase"><span class="gold_border border_1 d-inline-block pl-4 pr-4 pt-2 pb-2">Lectura recomendada</span></h6>
                            <h2 class="ifgp deepBlue">Numerología del 2018</h2>
                            <p class="deepBlue0">2018 - a 2 year - is an auspicious year in numerology, especially in relationships and alliances. What will it bring for you?</p>
                            <footer><a href="#" title="Leer mas" class="gold">Leer más <i class="fas fa-arrow-right"></i></a></footer>
                        </article>
                    </aside>
                    <div class="col-md-8 col-lg-7 col-xs-12">
                        <article class="card post mb-4">
                            <header class="userControl p-4 border-bottom violet_border">
                                <figure class="user d-flex flex-row m-0 justify-content-start">
                                    <div class="circle mr-2"><img src="img/usuarios/2.jpg" alt="usuario 1" /></div>
                                    <figcaption>
                                        <h5 class="khand mb-0 mt-2 deepBlue">Pedro Luis Arco</h5>
                                        <p class="deepBlue0"> Totalmente deacuerdo como me siento hoy <i class="far fa-smile"></i> </p>
                                    </figcaption>
                                    <p class="timePost"><small class="text-uppercase deepBlue0">Hace 5 minutos</small></p>
                                </figure>
                            </header>
                            <div class="p-4">
                                <article class="sharedContent violet_border border_1 text-center p-4 mt-2">
                                    <header>
                                        <h3 class="violet khand text-uppercase"><span class="d-inline-block white_bg pl-4 pr-4">Libra</span></h3>
                                    </header>
                                    <p class="ifgp lead deepBlue0"><em>You might spend the morning accepting things as they are. Then you could spend your lunch hour imagining things as you would like them to be. Then, for a finale to your day, you might spend time in late afternoon and
                                        early evening making things the way you want them to be. It's going to take a lot of work, but when.</em></p>
                                </article>
                            </div>
                            <footer class="deepBlue00_bg text-center deepBlue d-flex flex-row justify-content-between pl-4 pr-4">
                                <p class="violet m-0 pt-1 pb-1"><span>16 <i class="fas fa-heart"></i></span><span>45 <i class="fas fa-share-alt"></i></span></p>
                                <p class="violet m-0 pt-1 pb-1">16 <i class="fas fa-comments"></i></p>
                            </footer>
                        </article>
                        <article class="card post mb-4">
                            <header class="userControl p-4 border-bottom violet_border">
                                <figure class="user d-flex flex-row m-0 justify-content-start">
                                    <div class="circle mr-2"><img src="img/usuarios/3.jpg" alt="usuario 1" /></div>
                                    <figcaption>
                                        <h5 class="khand mb-0 mt-2 deepBlue">Lisa Alvarez</h5>
                                        <p class="deepBlue0">Arianos al poder! <i class="far fa-smile"></i> </p>
                                    </figcaption>
                                    <p class="timePost"><small class="text-uppercase deepBlue0">Hace 15 minutos</small></p>
                                </figure>
                            </header>
                            <div class="p-4">
                                <article class="sharedContent violet_border border_1 text-center p-4 mt-2">
                                    <header>
                                        <h3 class="violet khand text-uppercase"><span class="d-inline-block white_bg pl-4 pr-4">Aries - Ascendente Escorpio</span></h3>
                                    </header>
                                    <p class="ifgp lead deepBlue0"><em>Soltate! Deja de pensar y libera tus impulsos. Es tiempo de hacer ese llamado que te tiene en vilo. Hoy vas a lograr todo lo que te propongas. Animos!!</em></p>
                                </article>
                            </div>
                            <footer class="deepBlue00_bg text-center deepBlue d-flex flex-row justify-content-between pl-4 pr-4">
                                <p class="violet m-0 pt-1 pb-1"><span>16 <i class="fas fa-heart"></i></span><span>45 <i class="fas fa-share-alt"></i></span></p>
                                <p class="violet m-0 pt-1 pb-1">16 <i class="fas fa-comments"></i></p>
                            </footer>
                        </article>
                        <article class="card post mb-4">
                            <header class="userControl p-4 border-bottom violet_border">
                                <figure class="user d-flex flex-row m-0 justify-content-start">
                                    <div class="circle mr-2"><img src="img/usuarios/1.jpg" alt="usuario 1" /></div>
                                    <figcaption>
                                        <h5 class="khand mb-0 mt-2 deepBlue">Pedro Luis Arco</h5>
                                        <p class="deepBlue0"> Totalmente deacuerdo como me siento hoy <i class="far fa-smile"></i> </p>
                                    </figcaption>
                                    <p class="timePost"><small class="text-uppercase deepBlue0">Hace 5 minutos</small></p>
                                </figure>
                            </header>
                            <div class="p-4">
                                <article class="sharedContent violet_border border_1 text-center p-4 mt-2">
                                    <header>
                                        <h3 class="violet khand text-uppercase"><span class="d-inline-block white_bg pl-4 pr-4">Libra</span></h3>
                                    </header>
                                    <p class="ifgp lead deepBlue0"><em>You might spend the morning accepting things as they are. Then you could spend your lunch hour imagining things as you would like them to be. Then, for a finale to your day, you might spend time in late afternoon and
                                        early evening making things the way you want them to be. It's going to take a lot of work, but when.</em></p>
                                </article>
                            </div>
                            <footer class="deepBlue00_bg text-center deepBlue d-flex flex-row justify-content-between pl-4 pr-4">
                                <p class="violet m-0 pt-1 pb-1"><span>16 <i class="fas fa-heart"></i></span><span>45 <i class="fas fa-share-alt"></i></span></p>
                                <p class="violet m-0 pt-1 pb-1">16 <i class="fas fa-comments"></i></p>
                            </footer>
                        </article>
                    </div>
                    <div class="col-1 chat d-none d-lg-block">
                        <ul class="list-unstyled">
                            <li class="text-center active mb-2 circle">
                                <figure class="circle">
                                    <img src="img/usuarios/1.jpg" class="mx-auto d-block" alt="Usuario 1" />
                                </figure>
                            </li>
                            <li class="text-center mb-2 circle">
                                <figure class="circle">
                                    <img src="img/usuarios/2.jpg" class="mx-auto d-block" alt="Usuario 1" />
                                </figure>
                            </li>
                            <li class="text-center mb-2 circle">
                                <figure class="circle">
                                    <img src="img/usuarios/3.jpg" class="mx-auto d-block" alt="Usuario 1" />
                                </figure>
                            </li>
                            <li class="text-center active mb-2 circle">
                                <figure class="circle">
                                    <img src="img/usuarios/1.jpg" class="mx-auto d-block" alt="Usuario 1" />
                                </figure>
                            </li>
                            <li class="text-center mb-2 circle">
                                <figure class="circle">
                                    <img src="img/usuarios/2.jpg" class="mx-auto d-block" alt="Usuario 1" />
                                </figure>
                            </li>
                            <li class="text-center mb-2 circle">
                                <figure class="circle">
                                    <img src="img/usuarios/3.jpg" class="mx-auto d-block" alt="Usuario 1" />
                                </figure>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>
        </div>

</main>
        <?php
    include("includes/footer.php");
?>