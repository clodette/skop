<?php
    include('globales.php');
    require_once('clases/model.php');
    require_once('clases/user.php');

    session_start();
    define( 'CURRENT_SECTION', 'login' );


    if (parameterExist('user', $_SESSION)){
      header("Location: index.php");
      exit;
    }

    $initialUser = [
      'email' => returnInfo('email', $_POST),
      'password' => returnInfo('password', $_POST),
    ];

    $error = false;
    $passwordError = false;
    $exist_user = NULL;


    if(parameterExist('submit', $_POST))
    {
      if($initialUser['email']!= NULL && $initialUser['password'] != NULL)
      {
        $exist_user = new User($initialUser);
      }
      else
      {
        $error = true;
      }
    }

    $error_exist = false;

    if($exist_user!= NULL)
    {
      $user_record = $exist_user->find('email', $initialUser['email']);

      if ($user_record)
      {
        if (password_verify($initialUser['password'], $user_record['Password']))
        {
          $_SESSION['user'] = $initialUser['email'];
          header("Location: index.php");
          exit;
        }
        else
        {
        $passwordError = true;
        }
      }
      else
      {
        $error_exist = true;
      }
    };

    include("includes/header.php");
?>
    <main role="main">

        <div class="container mainContent d-flex flex-column justify-content-center align-items-center">
            <section class="registerForm col-lg-6 col-sm-12">
                <header>
                    <h1 class="gray-dark text-center">Ingreso</h1>
                </header>
                <div class="card p-4">
                    <p class="text-center ifgp lead m-0  deepBlue"><em>Ingresa a <em class="khand bold normal">SKOP.COM</em> y divertite con gente astrológicamente afin a vos!</em>
                    </p>
                    <?php if($error_exist): ?>
                        <div class="col-md-12 alerta">
                        El usuario no existe. <a href="registro.php"><strong class="violet">Registrate aquí</strong></a>
                      </div>
                    <?php endif; ?>

                    <?php if($passwordError): ?>
                      <div class="col-md-12 alerta">
                        La clave es invalida. Intentelo de nuevo
                      </div>
                    <?php endif; ?>

                    <form class="mt-2" method="post">
                        <div class="form-group">
                            <label for="email" class="text-uppercase violet">Email *</label>
                            <?php
                              $error_class = '';
                              if($error && !$initialUser['email']) {$error_class = 'error_class';};
                            ?>
                            <input type="email" class="form-control <?=$error_class?>" id="email" name="email" value="<?= $initialUser['email']?>">
                            <?php if($error && $initialUser['email'] == null):?>
                              <span class="error_message">Ingrese su email</span>
                            <?php endif; ?>

                        </div>
                        <div class="form-group">
                            <label for="password" class="text-uppercase violet">Contraseña *</label>
                            <?php
                              $error_class = '';
                              if($error && !$initialUser['password']) {$error_class = 'error_class';};
                            ?>
                            <input type="password" class="form-control <?=$error_class?>" id="password" name="password" value="">
                            <?php if($error && $initialUser['password'] == null):?>
                              <span class="error_message">Ingrese su password</span>
                            <?php endif; ?>
                        </div>
                        <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                            <label class="form-check-label text-uppercase violet" for="defaultCheck1">
                                Recordarme
                            </label>
                            </div>
                            <div class="d-flex">
                                <p class="deepBlue0 mb-2 text-right col"><small>Si olvidaste tu contraseña hace click <a href="#" class="deepBlue0">aquí</a></small></p>
                            </div>
                        </div>
                        <div class="row text-center d-flex justify-content-center align-items-center">
                            <button type="submit" name="submit" class="btn btn-primary gold_bg gold_border">Ingresar</button>
                        </div>
                    </form>
                </div>
                <hr class="separator" style= "margin-top: 3rem"/>
                <p class="purple text-center">Si no tienes una cuenta, <a href="registro.php" class="violet">registrate aquí</a></p>
            </section>
        </div>

</main>
        <?php
    include("includes/footer.php");
?>
