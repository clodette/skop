<?php
    define( 'CURRENT_SECTION', 'faqs' );
    include("includes/header.php");
    
?>
    <main role="main">

        <div class="container mainContent d-flex flex-column justify-content-start align-items-center">
            <section class="registerForm col-lg-10 col-sm-12">
                <header>
                    <h1 class="white text-center">Preguntas frecuentes</h1>
                    <p class="text-center ifgp lead m-0  white"><em>Aquí encontrarás información sobre nuestra plataforma.</p>
                        <p class="text-center ifgp lead m-0  white">Por consultas adicionales, escribinos a este correo:<a href:"mailto:info@skop.com" title="Escribinos!" class="white">info@skop.com.</a> ¡Te responderemos a la brevedad!</em></p>
                </header>
                <div class="p-2 mt-2">
                    <div class="d-flex row justify-content-around">
                        <article class="col-lg-4 col-sm-6 col-xs-12 mb-4">
                            <div class="card p-4">
                                <header>
                                    <h4 class="khand text-center violet text-uppercase bold">Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h4>
                                </header>
                                <p>Donec viverra metus ut mauris maximus tristique. Phasellus vitae sagittis diam. Sed varius lectus velit, ac porta nulla lacinia et. Curabitur viverra sapien eu enim vulputate scelerisque. Fusce vel elit ultricies, mattis
                                    ipsum a, mattis arcu.</p>
                            </div>
                        </article>
                        <article class="col-lg-4 col-sm-6 col-xs-12 mb-4">
                            <div class="card p-4">
                                <header>
                                    <h4 class="khand text-center violet text-uppercase bold">Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h4>
                                </header>
                                <p>Donec viverra metus ut mauris maximus tristique. Phasellus vitae sagittis diam. Sed varius lectus velit, ac porta nulla lacinia et. Curabitur viverra sapien eu enim vulputate scelerisque. Fusce vel elit ultricies, mattis
                                    ipsum a, mattis arcu.</p>
                            </div>
                        </article>
                        <article class="col-lg-4 col-sm-6 col-xs-12 mb-4">
                            <div class="card p-4">
                                <header>
                                    <h4 class="khand text-center violet text-uppercase bold">Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h4>
                                </header>
                                <p>Donec viverra metus ut mauris maximus tristique. Phasellus vitae sagittis diam. Sed varius lectus velit, ac porta nulla lacinia et. Curabitur viverra sapien eu enim vulputate scelerisque. Fusce vel elit ultricies, mattis
                                    ipsum a, mattis arcu.</p>
                            </div>
                        </article>
                        <article class="col-lg-4 col-sm-6 col-xs-12 mb-4">
                            <div class="card p-4">
                                <header>
                                    <h4 class="khand text-center violet text-uppercase bold">Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h4>
                                </header>
                                <p>Donec viverra metus ut mauris maximus tristique. Phasellus vitae sagittis diam. Sed varius lectus velit, ac porta nulla lacinia et. Curabitur viverra sapien eu enim vulputate scelerisque. Fusce vel elit ultricies, mattis
                                    ipsum a, mattis arcu.</p>
                            </div>
                        </article>
                        <article class="col-lg-4 col-sm-6 col-xs-12 mb-4">
                            <div class="card p-4">
                                <header>
                                    <h4 class="khand text-center violet text-uppercase bold">Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h4>
                                </header>
                                <p>Donec viverra metus ut mauris maximus tristique. Phasellus vitae sagittis diam. Sed varius lectus velit, ac porta nulla lacinia et. Curabitur viverra sapien eu enim vulputate scelerisque. Fusce vel elit ultricies, mattis
                                    ipsum a, mattis arcu.</p>
                            </div>
                        </article>
                        <article class="col-lg-4 col-sm-6 col-xs-12 mb-4">
                            <div class="card p-4">
                                <header>
                                    <h4 class="khand text-center violet text-uppercase bold">Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h4>
                                </header>
                                <p>Donec viverra metus ut mauris maximus tristique. Phasellus vitae sagittis diam. Sed varius lectus velit, ac porta nulla lacinia et. Curabitur viverra sapien eu enim vulputate scelerisque. Fusce vel elit ultricies, mattis
                                    ipsum a, mattis arcu.</p>
                            </div>
                        </article>
                        <article class="col-lg-4 col-sm-6 col-xs-12 mb-4">
                            <div class="card p-4">
                                <header>
                                    <h4 class="khand text-center violet text-uppercase bold">Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h4>
                                </header>
                                <p>Donec viverra metus ut mauris maximus tristique. Phasellus vitae sagittis diam. Sed varius lectus velit, ac porta nulla lacinia et. Curabitur viverra sapien eu enim vulputate scelerisque. Fusce vel elit ultricies, mattis
                                    ipsum a, mattis arcu.</p>
                            </div>
                        </article>
                        <article class="col-lg-4 col-sm-6 col-xs-12 mb-4">
                            <div class="card p-4">
                                <header>
                                    <h4 class="khand text-center violet text-uppercase bold">Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h4>
                                </header>
                                <p>Donec viverra metus ut mauris maximus tristique. Phasellus vitae sagittis diam. Sed varius lectus velit, ac porta nulla lacinia et. Curabitur viverra sapien eu enim vulputate scelerisque. Fusce vel elit ultricies, mattis
                                    ipsum a, mattis arcu.</p>
                            </div>
                        </article>
                        <article class="col-lg-4 col-sm-6 col-xs-12 mb-4">
                            <div class="card p-4">
                                <header>
                                    <h4 class="khand text-center violet text-uppercase bold">Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h4>
                                </header>
                                <p>Donec viverra metus ut mauris maximus tristique. Phasellus vitae sagittis diam. Sed varius lectus velit, ac porta nulla lacinia et. Curabitur viverra sapien eu enim vulputate scelerisque. Fusce vel elit ultricies, mattis
                                    ipsum a, mattis arcu.</p>
                            </div>
                        </article>
                        <article class="col-lg-4 col-sm-6 col-xs-12 mb-4">
                            <div class="card p-4">
                                <header>
                                    <h4 class="khand text-center violet text-uppercase bold">Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h4>
                                </header>
                                <p>Donec viverra metus ut mauris maximus tristique. Phasellus vitae sagittis diam. Sed varius lectus velit, ac porta nulla lacinia et. Curabitur viverra sapien eu enim vulputate scelerisque. Fusce vel elit ultricies, mattis
                                    ipsum a, mattis arcu.</p>
                            </div>
                        </article>
                        <article class="col-lg-4 col-sm-6 col-xs-12 mb-4">
                            <div class="card p-4">
                                <header>
                                    <h4 class="khand text-center violet text-uppercase bold">Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h4>
                                </header>
                                <p>Donec viverra metus ut mauris maximus tristique. Phasellus vitae sagittis diam. Sed varius lectus velit, ac porta nulla lacinia et. Curabitur viverra sapien eu enim vulputate scelerisque. Fusce vel elit ultricies, mattis
                                    ipsum a, mattis arcu.</p>
                            </div>
                        </article>
                        <article class="col-lg-4 col-sm-6 col-xs-12 mb-4">
                            <div class="card p-4">
                                <header>
                                    <h4 class="khand text-center violet text-uppercase bold">Lorem ipsum dolor sit amet, consectetur adipiscing elit?</h4>
                                </header>
                                <p>Donec viverra metus ut mauris maximus tristique. Phasellus vitae sagittis diam. Sed varius lectus velit, ac porta nulla lacinia et. Curabitur viverra sapien eu enim vulputate scelerisque. Fusce vel elit ultricies, mattis
                                    ipsum a, mattis arcu.</p>
                            </div>
                        </article>



                    </div>
                </div>
            </section>
        </div>
    </main>

    <?php
    include("includes/footer.php");
?>